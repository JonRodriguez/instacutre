//
//  TableViewController.m
//
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import "TableViewController.h"
#import "TVCell.h"
#import "FilterViewController.h"
#import "FaceDetectionViewController.h"

@interface TableViewController ()

@end

@implementation TableViewController

@synthesize details;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithDetails:(PictureCoredata *)theDetails
{
    self = [super init];
    if (self) {
        self.details = theDetails;
        self.title = @"Detalles";
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"TVCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TVCell"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 550;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TVCell";
    TVCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    NSData *pngData = [NSData dataWithContentsOfFile:self.details.photo];
    UIImage *image = [UIImage imageWithData:pngData];
    // Configure the cell...
    [cell.detailPhoto setImage:image];
    [cell.detailLatitude setText:self.details.latitude];
    [cell.detailLongitude setText:self.details.longitude];
    [cell.detailAlture setText:self.details.altitude];
    
    cell.delegate = self;
    cell.filtersDelegate = self;
    cell.faceDelegate = self;
    
    
    return cell;
}

#pragma mark - Compartir en redes sociales

- (void)sharePhoto:(UIActivityViewController *) ActivityView {
    
    [self presentViewController:ActivityView animated:YES completion:nil];
    
}

#pragma mark - Ventana de filtros

-(void)filtros{
    FilterViewController *filterView = [[FilterViewController alloc] initWithDetails:self.details];
    [self.navigationController pushViewController:filterView animated:YES];
}

#pragma mark - Ventana de detecion de caras

-(void)faceDetect{
    FaceDetectionViewController *faceDetectView = [[FaceDetectionViewController alloc] initWithDetails:self.details];
    [self.navigationController pushViewController:faceDetectView animated:YES];
}

@end
