//
//  FilterViewController.m
//  
//
//  Created by Chicken-Mac on 25/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import "FilterViewController.h"


@interface FilterViewController ()

@property (nonatomic, strong) CIImage *filterPreviewImage;
@property (weak, nonatomic) IBOutlet UIImageView *photo;

@end

@implementation FilterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithDetails:(PictureCoredata *)theDetails
{
    self = [super init];
    if (self) {
        self.details = theDetails;
        self.title = @"Filtros";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSData *pngData = [NSData dataWithContentsOfFile:self.details.photo];
    UIImage *image = [UIImage imageWithData:pngData];
    [self.photo setImage:image];
    self.filterPreviewImage = [[CIImage alloc] initWithImage:image];
    
    UIBarButtonItem *showSettingsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveFilter:)];
    
    self.navigationItem.rightBarButtonItem = showSettingsButton;
    
}


-(void)saveFilter:(id)sender
{
    NSData *imageData = UIImagePNGRepresentation(self.photo.image);
    [imageData writeToFile:self.details.photo atomically:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)filtroSepia:(id)sender {
    
    CIContext *imgContext = [CIContext contextWithOptions:nil];
    
    CIFilter *imgFilter = [CIFilter filterWithName:@"CISepiaTone" keysAndValues:kCIInputImageKey,self.filterPreviewImage,
                             @"inputIntensity",[NSNumber numberWithFloat:0.6],nil];
    
    CIImage *myOutputImage = [imgFilter outputImage];
    
    CGImageRef cgImgRef = [imgContext  createCGImage:myOutputImage fromRect:[myOutputImage extent]];
    UIImage *newImgWithFilter = [UIImage imageWithCGImage:cgImgRef];
    
    [self.photo setImage:newImgWithFilter];
    
    CGImageRelease(cgImgRef);

}

- (IBAction)filtroPixel:(id)sender {
    CIContext *imgContext = [CIContext contextWithOptions:nil];
    
    CIFilter *imgFilter = [CIFilter filterWithName:@"CIPixellate" keysAndValues:kCIInputImageKey,self.filterPreviewImage, nil];
    
    CIImage *myOutputImage = [imgFilter outputImage];
    
    CGImageRef cgImgRef = [imgContext  createCGImage:myOutputImage fromRect:[myOutputImage extent]];
    UIImage *newImgWithFilter = [UIImage imageWithCGImage:cgImgRef];
    
    [self.photo setImage:newImgWithFilter];
    
    CGImageRelease(cgImgRef);
}

- (IBAction)filtroEffectChrome:(id)sender {
    CIContext *imgContext = [CIContext contextWithOptions:nil];
    
    CIFilter *imgFilter = [CIFilter filterWithName:@"CIPhotoEffectChrome" keysAndValues:kCIInputImageKey,self.filterPreviewImage,nil];
    
    CIImage *myOutputImage = [imgFilter outputImage];
    
    CGImageRef cgImgRef = [imgContext  createCGImage:myOutputImage fromRect:[myOutputImage extent]];
    UIImage *newImgWithFilter = [UIImage imageWithCGImage:cgImgRef];
    
    [self.photo setImage:newImgWithFilter];
    
    CGImageRelease(cgImgRef);
}

- (IBAction)filtroMono:(id)sender {
    CIContext *imgContext = [CIContext contextWithOptions:nil];
    
    CIFilter *imgFilter = [CIFilter filterWithName:@"CIPhotoEffectTonal" keysAndValues:kCIInputImageKey,self.filterPreviewImage,nil];
    
    CIImage *myOutputImage = [imgFilter outputImage];
    
    CGImageRef cgImgRef = [imgContext  createCGImage:myOutputImage fromRect:[myOutputImage extent]];
    UIImage *newImgWithFilter = [UIImage imageWithCGImage:cgImgRef];
    
    [self.photo setImage:newImgWithFilter];
    
    CGImageRelease(cgImgRef);
}

- (IBAction)filtroVignetteEffect:(id)sender {
    CIContext *imgContext = [CIContext contextWithOptions:nil];
    NSData *pngData = [NSData dataWithContentsOfFile:self.details.photo];
    UIImage *image = [UIImage imageWithData:pngData];
    
    CIFilter *imgFilter = [CIFilter filterWithName:@"CIVignetteEffect" keysAndValues:kCIInputImageKey,self.filterPreviewImage,
                           @"inputIntensity",[NSNumber numberWithFloat:0.5],
                           @"inputCenter", [[CIVector alloc] initWithX:image.size.width * 0.5 Y:image.size.height * (1 - 0.5)],nil];
    
    CIImage *myOutputImage = [imgFilter outputImage];
    
    CGImageRef cgImgRef = [imgContext  createCGImage:myOutputImage fromRect:[myOutputImage extent]];
    UIImage *newImgWithFilter = [UIImage imageWithCGImage:cgImgRef];
    
    [self.photo setImage:newImgWithFilter];
    
    CGImageRelease(cgImgRef);
}

@end
