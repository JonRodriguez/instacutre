//
//  TableViewController.h
//  
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureCoredata.h"
#import "TVCell.h"

@interface TableViewController : UITableViewController <shareProtocol, filtersProtocol, faceProtocol>

-(id) initWithDetails:(PictureCoredata *) theDetails;

@property (nonatomic,weak) PictureCoredata *details;

@end
