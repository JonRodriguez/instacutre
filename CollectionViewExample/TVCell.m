//
//  TVCell.m
//  
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import "TVCell.h"

@implementation TVCell


- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)compartir:(id)sender {
    
    UIImage *anImage = self.detailPhoto.image;
    NSArray *Items   = [NSArray arrayWithObjects: anImage, nil, nil];
    
    UIActivityViewController *ActivityView =
    [[UIActivityViewController alloc]
     initWithActivityItems:Items applicationActivities:nil];
    
    [self.delegate sharePhoto:ActivityView];
    
}

-(IBAction)filtros:(id)sender{
    
    [self.filtersDelegate filtros];
}

-(IBAction)faceDetect:(id)sender{
    
    [self.faceDelegate faceDetect];
}

@end
