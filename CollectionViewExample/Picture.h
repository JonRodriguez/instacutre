//
//  Picture.h
//  
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Picture : NSObject

-(id) initWithPicture:(NSString *) thePicture andLatitude:(NSString *) theLatitude andLongitude:(NSString *) theLongitude andAltitude:(NSString *) theAltitude andArrayPosition: (NSNumber *) theCounter andFacesArray: (NSData *) theFacesArray;

@property (nonatomic,strong) NSString *photo;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *altitude;
@property (nonatomic, strong) NSNumber *contador;
@property (nonatomic, strong) NSData *facesArray;

@end
