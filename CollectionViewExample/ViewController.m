

#import "ViewController.h"
#import "CVCell.h"
#import "Picture.h"
#import "TableViewController.h"
#import "AppDelegate.h"
#import "PictureCoredata.h"

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) CLLocationManager *manager;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSMutableArray *firstSection;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *altitude;
@property (nonatomic) int contadorArray;

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

@end

@implementation ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"InstaCutre";
        NSShadow *shadow = [NSShadow.alloc init];
        //color verde para el navBar title
        shadow.shadowColor = [UIColor clearColor];
        NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:0.106 green:0.404 blue:0.42 alpha:1], NSForegroundColorAttributeName, shadow, NSShadowAttributeName, nil];
        [[UINavigationBar appearance] setTitleTextAttributes:textTitleOptions];

    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //rellenamos array con info de coreData
    AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    self.dataArray = [appDelegate getAllDataBase];
    
    self.firstSection = [[NSMutableArray alloc] init];
    self.contadorArray = [self.dataArray count]-1;
    for (int i = 0; i <= self.contadorArray; i++)
    {
        [self.firstSection addObject:[self.dataArray objectAtIndex:i]];
    }
    self.dataArray = [[NSArray alloc] initWithObjects:self.firstSection, nil];
    
    self.latitude = [NSString stringWithFormat:@"0.0"];
    self.longitude =[NSString stringWithFormat:@"0.0"];
    self.altitude =[NSString stringWithFormat:@"0.0"];
    
    /*-----------------------------------*/
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
//    NSString *imagePath = [documentsPath stringByAppendingPathComponent:@"Kat_Dennings.jpg"];
//    NSData *facesArray = [self facesDetection:[UIImage imageNamed:@"Kat_Dennings.jpg"]];
//    [self.firstSection addObject:[[Picture alloc]initWithPicture:imagePath andLatitude:self.latitude andLongitude:self.longitude andAltitude:self.altitude andArrayPosition:self.contadorArray andFacesArray:facesArray]];
//    self.contadorArray++;
//    NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:@"Kat_Dennings.jpg"]);
//    [imageData writeToFile:imagePath atomically:YES];
//    
//    self.dataArray = [[NSArray alloc] initWithObjects:self.firstSection, nil];
    /*----------------------------------*/
    
    [self.collectionView registerClass:[CVCell class] forCellWithReuseIdentifier:@"cvCell"];
    
    // Configure layout
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(100, 100)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    //color para el navBar
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0.106 green:0.404 blue:0.42 alpha:1];
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView functions

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [self.dataArray count];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    NSMutableArray *sectionArray = [self.dataArray objectAtIndex:section];
    return [sectionArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    // Setup cell identifier
    static NSString *cellIdentifier = @"cvCell";
    
    
    CVCell *cell = (CVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    NSMutableArray *data = [self.dataArray objectAtIndex:indexPath.section];
    
    PictureCoredata *cellData = [data objectAtIndex:indexPath.row];
    
    NSData *pngData = [NSData dataWithContentsOfFile:cellData.photo];
    UIImage *image = [UIImage imageWithData:pngData];
    
    [cell.imageThum setImage:[self dataThumbnailFromImage:image]];

 
    // Return the cell
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"colection view selected");
    NSMutableArray *data = [self.dataArray objectAtIndex:indexPath.section];
    
    PictureCoredata *cellData = [data objectAtIndex:indexPath.row];
    
    TableViewController *detailsTable = [[TableViewController alloc] initWithDetails:cellData];
    [self.navigationController pushViewController:detailsTable animated:YES];
}

#pragma mark - Camera functions

- (IBAction)takePhoto:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        if (self.manager!=nil) {
            if ([CLLocationManager locationServicesEnabled]) {
                [self.manager startUpdatingLocation];
            }
        }
        if ([CLLocationManager locationServicesEnabled]) {
            self.manager = [[CLLocationManager alloc] init];
            self.manager.desiredAccuracy = kCLLocationAccuracyBest;
            self.manager.delegate = self;
            [self.manager startUpdatingLocation];
        }
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.showsCameraControls = YES;
        picker.allowsEditing = NO;
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No uses el simulador, cacho bestia" delegate:nil cancelButtonTitle:@"Vale, vale" otherButtonTitles: nil] show];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    //obtener el directorio
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileName = [NSString stringWithFormat:@"photo-%04d.jpg", self.contadorArray];
    NSString *imagePath = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    //guadar foto en directorio
    UIImage *originalPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    //corregir orientacion de la imagen
    originalPhoto = [self rotate:originalPhoto andOrientation:originalPhoto.imageOrientation];
    
    NSData *imageData = UIImagePNGRepresentation(originalPhoto);
    [imageData writeToFile:imagePath atomically:YES];
    
    NSData *facesArray = [self facesDetection:originalPhoto];
    // Create data for collection views
    Picture * newPicture = [[Picture alloc]initWithPicture:imagePath andLatitude:self.latitude andLongitude:self.longitude andAltitude:self.altitude andArrayPosition:[NSNumber numberWithInteger:self.contadorArray] andFacesArray:facesArray];
    [self.firstSection addObject:newPicture];
    self.contadorArray++;
    
    self.dataArray = [[NSArray alloc] initWithObjects:self.firstSection, nil];
    //guardamos en base de datos
    [self guardarCoreData:newPicture];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage*) rotate:(UIImage*) src andOrientation:(UIImageOrientation)orientation
{
    UIGraphicsBeginImageContext(src.size);
    
    CGContextRef context=(UIGraphicsGetCurrentContext());
    
    if (orientation == UIImageOrientationRight) {
        CGContextRotateCTM (context, 90/180*M_PI) ;
    } else if (orientation == UIImageOrientationLeft) {
        CGContextRotateCTM (context, -90/180*M_PI);
    } else if (orientation == UIImageOrientationDown) {
        // NOTHING
    } else if (orientation == UIImageOrientationUp) {
        CGContextRotateCTM (context, 90/180*M_PI);
    }
    
    [src drawAtPoint:CGPointMake(0, 0)];
    UIImage *img=UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
    
}

-(UIImage *) dataThumbnailFromImage: (UIImage *) originalImage
{
    CGSize destinationSize = CGSizeMake(originalImage.size.width / 8, originalImage.size.height / 8);;
    UIGraphicsBeginImageContext(destinationSize);
    [originalImage drawInRect:CGRectMake(0,0,destinationSize.width,destinationSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    NSData *ret;
    if (!newImage)
    {
        newImage = [UIImage imageNamed:@"instacutre logo 120x120.png"] ;
        ret = UIImagePNGRepresentation(newImage);
    }
    else
    {
        ret = UIImagePNGRepresentation(newImage);
    }
    return [UIImage imageWithData:ret];
}


#pragma mark - CLLocationManager updates
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *lastLocation = [locations lastObject];
    self.latitude = [NSString stringWithFormat:@"%.4f", lastLocation.coordinate.latitude];
    self.longitude = [NSString stringWithFormat:@"%.4f", lastLocation.coordinate.longitude];
    self.altitude = [NSString stringWithFormat:@"%.4f", lastLocation.altitude];
    
    [self.manager stopUpdatingLocation];
}


#pragma mark - Face detection
-(NSData *) facesDetection: (UIImage *) image{
    
    CIImage *myImg = [[CIImage alloc]initWithImage:image];
    CIContext *context = [CIContext contextWithOptions:nil];
    NSDictionary *opts = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
    
    CIDetector *face = [CIDetector detectorOfType:CIDetectorTypeFace context:context options:opts];
    
    NSArray *facesDetector = [face featuresInImage:myImg];
    
    NSMutableArray *facesArr = [[NSMutableArray alloc]init];;
    
    for (CIFaceFeature *f in facesDetector) {
        NSString *s = NSStringFromCGRect(f.bounds);
        [facesArr addObject: s];
    }
    return [NSKeyedArchiver archivedDataWithRootObject:facesArr];
}

#pragma mark - guardar CoreData

- (void)guardarCoreData: (Picture *) picture
{
    //  instanciando modelo
    PictureCoredata * newEntry = [NSEntityDescription insertNewObjectForEntityForName:@"PictureCoredata" inManagedObjectContext:self.managedObjectContext];
    //  rellenando modelo
    newEntry.photo = picture.photo;
    newEntry.latitude = picture.latitude;
    newEntry.longitude = picture.longitude;
    newEntry.altitude = picture.altitude;
    newEntry.contador  = [NSNumber numberWithInteger:picture.contador];
    newEntry.facesArray = picture.facesArray;
    //  guardando en base de datos
    NSError *error;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}


@end
