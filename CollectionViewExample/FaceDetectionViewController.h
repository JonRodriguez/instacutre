//
//  FaceDetectionViewController.h
//  
//
//  Created by Chicken-Mac on 26/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PictureCoredata.h"

@interface FaceDetectionViewController : UIViewController

-(id) initWithDetails:(PictureCoredata *) theDetails;

@property (nonatomic,weak) PictureCoredata *details;

@end
