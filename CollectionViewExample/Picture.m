//
//  Picture.m
//  
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import "Picture.h"

@implementation Picture

@synthesize photo;
@synthesize latitude;
@synthesize longitude;
@synthesize altitude;
@synthesize contador;
@synthesize facesArray;

-(id) initWithPicture:(NSString *)thePicture andLatitude:(NSString *)theLatitude andLongitude:(NSString *)theLongitude andAltitude:(NSString *)theAltitude andArrayPosition:(NSNumber *)theCounter andFacesArray:(NSData *)theFacesArray
{
    self = [super init];
    
    self.photo = thePicture;
    self.latitude = theLatitude;
    self.longitude = theLongitude;
    self.altitude = theAltitude;
    self.contador = theCounter;
    self.facesArray = theFacesArray;
    
    
    return self;
}

@end
