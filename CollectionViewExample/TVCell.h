//
//  TVCell.h
//  
//
//  Created by Chicken-Mac on 24/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol shareProtocol <NSObject>
-(void)sharePhoto:(UIActivityViewController *)controller;
@end

@protocol filtersProtocol <NSObject>
-(void)filtros;
@end

@protocol faceProtocol <NSObject>
-(void)faceDetect;
@end


@interface TVCell : UITableViewCell

@property (nonatomic, retain) id<shareProtocol> delegate;
@property (nonatomic, retain) id<filtersProtocol> filtersDelegate;
@property (nonatomic, retain) id<faceProtocol> faceDelegate;

@property (weak, nonatomic) IBOutlet UIImageView *detailPhoto;
@property (weak, nonatomic) IBOutlet UILabel *detailLatitude;
@property (weak, nonatomic) IBOutlet UILabel *detailLongitude;
@property (weak, nonatomic) IBOutlet UILabel *detailAlture;

@end
