//
//  FaceDetectionViewController.m
//  
//
//  Created by Chicken-Mac on 26/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "FaceDetectionViewController.h"

@interface FaceDetectionViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *photo;
@property (strong, nonatomic) UIImage *image;

@end

@implementation FaceDetectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id) initWithDetails:(PictureCoredata *)theDetails
{
    self = [super init];
    if (self) {
        self.details = theDetails;
        self.title = @"Face Detect";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSData *pngData = [NSData dataWithContentsOfFile:self.details.photo];
    self.image = [UIImage imageWithData:pngData];
    [self faceDetector];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Detecion de caras

-(void)faceDetector
{        
    NSMutableArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:self.details.facesArray];
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1, -1);
    transform = CGAffineTransformTranslate(transform, 0,-self.image.size.height);
    UIImage *nImage = self.image;
    self.photo.image = nImage;
    for (NSString *s in array){
        CGRect r = CGRectApplyAffineTransform(CGRectFromString(s),transform);
        nImage = [self imageByDrawingRectOnImage:nImage rect:r];
        self.photo.image = nImage;
    }
}

- (UIImage *)imageByDrawingRectOnImage:(UIImage *)image rect:(CGRect ) rect
{
	UIGraphicsBeginImageContext(image.size);
	[image drawAtPoint:CGPointZero];
    
	CGContextRef ctx = UIGraphicsGetCurrentContext();
    
	[[UIColor redColor] setStroke];
    CGContextStrokeRectWithWidth(ctx, rect, 4);
    
	UIImage *retImage = UIGraphicsGetImageFromCurrentImageContext();
    
	UIGraphicsEndImageContext();
    
	return retImage;
}

@end
