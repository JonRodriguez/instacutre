

#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "FilterViewController.h"


@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>

@end
