//
//  PictureCoredata.h
//  InstaCutre
//
//  Created by Chicken-Mac on 29/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PictureCoredata : NSManagedObject

-(id) initWithPicture:(NSString *) thePicture andLatitude:(NSString *) theLatitude andLongitude:(NSString *) theLongitude andAltitude:(NSString *) theAltitude andArrayPosition: (NSNumber *) theCounter andFacesArray: (NSData *) theFacesArray;

@property (nonatomic, retain) NSString * altitude;
@property (nonatomic, retain) NSNumber * contador;
@property (nonatomic, retain) NSData * facesArray;
@property (nonatomic, retain) NSString * latitude;
@property (nonatomic, retain) NSString * longitude;
@property (nonatomic, retain) NSString * photo;

@end
