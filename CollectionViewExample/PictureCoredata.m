//
//  PictureCoredata.m
//  InstaCutre
//
//  Created by Chicken-Mac on 29/05/14.
//  Copyright (c) 2014 Charismatic Megafauna Ltd. All rights reserved.
//

#import "PictureCoredata.h"


@implementation PictureCoredata

@dynamic altitude;
@dynamic contador;
@dynamic facesArray;
@dynamic latitude;
@dynamic longitude;
@dynamic photo;

-(id) initWithPicture:(NSString *)thePicture andLatitude:(NSString *)theLatitude andLongitude:(NSString *)theLongitude andAltitude:(NSString *)theAltitude andArrayPosition:(NSNumber *)theCounter andFacesArray:(NSData *)theFacesArray
{
    self = [super init];
    
    self.photo = thePicture;
    self.latitude = theLatitude;
    self.longitude = theLongitude;
    self.altitude = theAltitude;
    self.contador = theCounter;
    self.facesArray = theFacesArray;
    
    
    return self;
}

@end
